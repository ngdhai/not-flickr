import { combineReducers } from 'redux';
import photoReducer from './Photo/photo.reducer';
import tagReducer from './Tag/tag.reducer';

const rootReducer = combineReducers({
  photoState: photoReducer,
  tagState: tagReducer
});

export default rootReducer;