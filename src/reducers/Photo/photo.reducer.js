import { PHOTOS_ADD, PHOTOS_FETCH_ERROR, PHOTO_SHOW_DETAIL, PHOTOS_CLEAR, PHOTOS_FETCH } from '../../constants/action-type';

const INITIAL_STATE = {
  photo: [],
  error: null
};

const applyAddPhotos = (state, action) => ({
  ...action.photos,
  photo: [ ...(state.photo || []),...action.photos.photo],
  isLoading: false,
  error: null
});

const applyFetchPhotosError = (state, action) => ({
  photos: {},
  error: action.error
});

const applyShowPhotoDetial = (state, action) => ({
  ...state,
  singlePhoto: action.photo
});

const applyClearPhotos = (state, action) => ({
  photo: [],
  error: null
});

const applyFetchPhotos = (state, action) => ({
  ...state,
  isLoading: true
});

const photoReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case PHOTOS_ADD: {
      return applyAddPhotos(state, action);
    }
    case PHOTOS_FETCH_ERROR: {
      return applyFetchPhotosError(state, action);
    }
    case PHOTO_SHOW_DETAIL: {
      return applyShowPhotoDetial(state, action);
    }
    case PHOTOS_CLEAR: {
      return applyClearPhotos(state, action);
    }
    case PHOTOS_FETCH: {
      return applyFetchPhotos(state, action);
    }
    default: return state;
  }
}

export default photoReducer;