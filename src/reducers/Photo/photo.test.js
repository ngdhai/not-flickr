import photoReducer from './photo.reducer';

describe('photo reducer', () => {
  it ('adds photos to the photo state', () => {
    const photos = ['a', 'b', 'c'];
    const action = {
      type: 'PHOTOS_ADD',
      photos
    }

    const previousState = {photos: [], error: null };
    const expectedNewState = { photos, error: null };

    const newState = photoReducer(previousState, action);
    expect(newState).toEqual(expectedNewState);
  });
  it('photos fetch error', () => {
    const action = {
      type: 'PHOTOS_FETCH_ERROR',
      error: 'some error'
    };

    const previousState = { photos: {}, error: null}
    const expectedNewState = {photos: {}, error: action.error}

    const newState = photoReducer(previousState, action);
    expect(newState).toEqual(expectedNewState);
  });

});