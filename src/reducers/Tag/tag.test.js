import tagReducer from './tag.reducer';

describe('tag reducer', () => {
  it ('adds tags to the tag state', () => {
    const tags = ['a', 'b', 'c'];
    const action = {
      type: 'TAGS_ADD',
      tags
    }

    const previousState = {tags: [] };
    const expectedNewState = { tags };

    const newState = tagReducer(previousState, action);
    expect(newState).toEqual(expectedNewState);
  })
});