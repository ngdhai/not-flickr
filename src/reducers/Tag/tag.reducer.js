import { TAGS_ADD } from '../../constants/action-type';

const INITIAL_STATE = {
  tags: []
};

const applyAddTags = (state, action) => ({
  tags: [...state.tags, ...action.tags]
});

const tagReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case TAGS_ADD: {
      return applyAddTags(state, action);
    }
    default: return state;
  }
}

export default tagReducer;