export const DEFAULT_ITEM_PER_PAGE = 20;
export const DEFAULT_PAGE = 1;
export const DEFAULT_EXTRAS = 'owner_name,views,url_n,url_l';
