import axios from 'axios';
import { FLICKR_API_KEY } from '../constants/api-key';
import { DEFAULT_PAGE, DEFAULT_ITEM_PER_PAGE, DEFAULT_EXTRAS } from '../constants/default-value';

const fetchInterestingPhotos = query => {
  const url = `https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=${FLICKR_API_KEY}&per_page=${DEFAULT_ITEM_PER_PAGE}&page=${query ? query.page : DEFAULT_PAGE}&format=json&extras=${DEFAULT_EXTRAS}&nojsoncallback=1`;
  console.log(url);
  return axios.get(url);
}

const fetchPhotosByTag = ({page, tag} = {page: 1}) => {
  console.log({page, tag})
  const url = tag ?
  `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${FLICKR_API_KEY}&per_page=${DEFAULT_ITEM_PER_PAGE}&page=${page || DEFAULT_PAGE}&tags=${tag}&sort=interestingness-desc&format=json&extras=${DEFAULT_EXTRAS}&nojsoncallback=1`:
  `https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=${FLICKR_API_KEY}&per_page=${DEFAULT_ITEM_PER_PAGE}&page=${page || DEFAULT_PAGE}&format=json&extras=${DEFAULT_EXTRAS}&nojsoncallback=1` ;
  
  console.log(url);
  return axios.get(url);
}

const fetchPhoto = (id) => {
  const url = `https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=${FLICKR_API_KEY}&photo_id=${id}&format=json&nojsoncallback=1`;
  console.log(url);
  return axios.get(url);
}

export {
  fetchInterestingPhotos,
  fetchPhotosByTag,
  fetchPhoto
}