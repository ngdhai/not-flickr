import { PHOTOS_FETCH, PHOTOS_FETCH_ERROR, PHOTOS_ADD, PHOTO_FETCH_DETAIL, PHOTO_SHOW_DETAIL, PHOTOS_CLEAR } from '../constants/action-type';

const doFetchPhotos = query => ({
  type: PHOTOS_FETCH,
  query
});

const doFetchPhotoDetail = query => ({
  type: PHOTO_FETCH_DETAIL,
  query
});

const doFetchPhotosError = error => ({
  type: PHOTOS_FETCH_ERROR,
  error
});

const doAddPhotos = photos => ({
  type: PHOTOS_ADD,
  photos
});

const doShowPhotoDetail = photo => ({
  type: PHOTO_SHOW_DETAIL,
  photo
});

const doClearPhotos = _ => ({
  type: PHOTOS_CLEAR
});

export {
  doFetchPhotos,
  doFetchPhotosError,
  doAddPhotos,
  doShowPhotoDetail,
  doFetchPhotoDetail,
  doClearPhotos
}