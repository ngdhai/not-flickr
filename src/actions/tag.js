import { TAGS_ADD } from '../constants/action-type';

const doAddTags = tags => ({
  type: TAGS_ADD,
  tags
});

export {
  doAddTags
}