import { call, put } from 'redux-saga/effects';
import { doAddPhotos, doFetchPhotosError, doShowPhotoDetail } from '../actions/photo';
import { doAddTags } from '../actions/tag';
import { fetchPhotosByTag, fetchPhoto } from '../api/flick';

function *handleFetchPhotos(action) {
  const { query } = action;
  try {
    const result = yield call(fetchPhotosByTag, query);
    yield put(doAddPhotos(result.data.photos));
    
  } catch (error) {
    yield put(doFetchPhotosError(error));
  }
};

function *handleFetchPhotoDetail(action) {
  const { query } = action;
  try {
    const result = yield call(fetchPhoto, query);
    yield put(doAddTags(result.data.photo.tags.tag));
    yield put(doShowPhotoDetail(result.data.photo));
  } catch (error) {
    yield put(doFetchPhotosError(error));
  }
}

export {
  handleFetchPhotos,
  handleFetchPhotoDetail
};