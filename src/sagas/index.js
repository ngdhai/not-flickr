import { takeEvery, all } from 'redux-saga/effects';
import { PHOTOS_FETCH, PHOTO_FETCH_DETAIL } from '../constants/action-type';
import { handleFetchPhotos, handleFetchPhotoDetail } from './photo';

function *watchAll() {
  yield all([
    takeEvery(PHOTOS_FETCH, handleFetchPhotos),
    takeEvery(PHOTO_FETCH_DETAIL, handleFetchPhotoDetail)
  ]);
};

export default watchAll;