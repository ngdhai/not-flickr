const getPhotos = ({ photoState }) => {
  return photoState.photo || [];
}

const getFetchError = ({ photoState }) => 
  photoState.error;

const getPage = ({ photoState }) => 
  photoState.page || 1;

const getHasMore = ({ photoState }) => 
  photoState.page < photoState.pages;

const getLoading = ({ photoState }) => 
  photoState.isLoading;

export {
  getPhotos,
  getFetchError,
  getPage,
  getHasMore,
  getLoading
}