import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import { AutoCompleteInput } from '..';

import './navbar.style.css';

const NavbarPresenter = ({ children, items, onToggle, isOpen, tags, onKeyPress, onInputChange, submit }) => 
  <div>
    <Navbar expand='md' className='dark-navbar'>
      <div className='navbar-container'>
        <NavbarBrand href='/' className='white-text brand-text'>{ children }</NavbarBrand>
        <NavbarToggler onClick={onToggle} />
        <Collapse isOpen={ isOpen } navbar>
          <Nav className='mr-auto' navbar>
            {
              items.map((item, i) =>
                <NavItem key={i}>
                  <Link to={item.link} className='white-text item-text nav-link'>{item.title}</Link>
                </NavItem>
              )
            }
          </Nav>
          <Nav className='ml-auto' navbar>
            <AutoCompleteInput suggestions={tags || []} onKeyPress={onKeyPress} onInputChange={onInputChange} submit={submit}/>
          </Nav>
        </Collapse>
      </div>
    </Navbar>
  </div>

export default NavbarPresenter;