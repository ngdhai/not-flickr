import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import NavbarPresenter from './navbar.presenter';
import { doFetchPhotos, doClearPhotos } from '../../actions/photo';


class NavbarContainer extends Component {
  constructor(props){
    super(props);

    this.state = {
      isOpen: false,
      searchTerm: ''
    }
  }

  onToggle = _ => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  onInputChange = newValue => {
    this.setState({ searchTerm: newValue });
  }

  onSubmit = _ => {
    const { history } = this.props;
    const { searchTerm } = this.state;
    history.push(`/tag/${searchTerm}`);

  }

  onInputValueChange = event => {
    this.setState({ searchTerm: event.target.value });
  }

  onKeyPress = event => {
    if (event.key === 'Enter') {
      this.props.onTagSubmit();
      this.onSubmit();
    }
  }

  render() {
    const { children, items, tags } = this.props;
    const { isOpen } = this.state;
    return (
      <NavbarPresenter
        items={items}
        isOpen={isOpen}
        onToggle={this.onToggle}
        tags={tags}
        onKeyPress={this.onKeyPress}
        onInputChange={this.onInputChange}
        submit={this.onSubmit}
      >
        {children}
      </NavbarPresenter>
    );
  }
}

const mapStateToProps = state => ({
  tags: state.tagState.tags
})

const mapDispatchToProps = dispatch => ({
  onFetchPhotos: query => dispatch(doFetchPhotos(query)),
  onTagSubmit: _ => dispatch(doClearPhotos())
});


export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(NavbarContainer)
);