import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import { Navbar, ImagesGrid, Photo } from '..';

import './app.style.css';

const App = _ => 
  <BrowserRouter>
    <div>
      <Navbar 
        items={
          [
            {title: '/expl0re', link: '/'},
          ]
        }
      >
        !flickr
      </Navbar>
      <Route exact path='/' component={ImagesGrid}></Route>
      <Route path='/tag/:search' component={ImagesGrid}></Route>
      <Route path='/photo/:id' component={Photo}></Route>
    </div>
  </BrowserRouter>

export default App;
