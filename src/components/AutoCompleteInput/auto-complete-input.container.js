import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';

import './auto-complete-input.style.css';

class AutoCompleteContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      suggestions: []
    };
  }

  onChange = (event, { newValue, method }) => {
    const { onInputChange } = this.props;
    onInputChange(newValue);
    this.setState({ value: newValue });
  }

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  }

  onSuggestionsClearRequested = _ => {
    this.setState({
      suggestions: []
    });
  }

  getSuggestions = value => {
    const { suggestions } = this.props;
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0 ? [] : suggestions.filter(tag =>
      tag.raw.toLowerCase().slice(0, inputLength) === inputValue
    );
  }

  getSuggestionValue = suggestion => 
    suggestion.raw;

  renderSuggestion = suggestion =>
    <div>
      { suggestion.raw }
    </div>

  render() {
    const { value, suggestions } = this.state;
    const { onKeyPress, submit } = this.props;
    const inputProps = {
      placeholder: 't4gs 0nly cuz this isn\'t flickr (ツ)',
      value,
      onChange: this.onChange,
      onKeyPress: onKeyPress
    };
    return (
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={this.getSuggestionValue}
          renderSuggestion={this.renderSuggestion}
          inputProps={inputProps}
          onSuggestionSelected={_ => submit()}
        />
    );
  }
}

export default AutoCompleteContainer;
