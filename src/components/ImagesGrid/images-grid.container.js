import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getPhotos, getFetchError, getPage, getHasMore, getLoading } from '../../selectors/photo';
import { doFetchPhotos, doClearPhotos } from '../../actions/photo';

import ImagesGridPresenter from './images-grid.presenter';

class ImagesGridContainer extends Component {
  onClickThumbnail = id => {
    this.props.history.push(`/photo/${id}`);
  }
  render() {
    const { photos } = this.props; 
    return (
      <ImagesGridPresenter
        photos={photos || []}
        onClickThumbnail={this.onClickThumbnail}
        onScroll={this.onScroll}
      />
    );
  }

  onScroll = _ => {
    const { isLoading, page, hasMore, onFetchPhotos } = this.props;
    const { search } = this.props.match.params;
    if (((window.innerHeight + window.scrollY) >= (document.body.offsetHeight)) && !isLoading && hasMore) {
      onFetchPhotos({ page: page + 1, tag: search });
    }

  }

  componentWillMount() {
    const { onClearPhotos } = this.props;
    onClearPhotos();
  }

  onScroll = props => {
    const { isLoading, page, hasMore, onFetchPhotos } =this.props;
    const { search } = this.props.match.params;
    if (((window.innerHeight + window.scrollY) >= (document.body.offsetHeight)) && !isLoading && hasMore) {
      onFetchPhotos({ page: page + 1, tag: search });
    }
  }


  componentDidMount() {
    const { onFetchPhotos } = this.props;
    const { search } = this.props.match.params;
    onFetchPhotos({page: 1, tag: search});
    window.addEventListener('scroll', this.onScroll, false);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }
}

const mapStateToProps = state => ({
  photos: getPhotos(state),
  page: getPage(state),
  hasMore: getHasMore(state),
  error: getFetchError(state),
  isLoading: getLoading(state)
});

const mapDispatchToProps = dispatch => ({
  onFetchPhotos: query => dispatch(doFetchPhotos(query)),
  onClearPhotos: _ => dispatch(doClearPhotos())
});


export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(ImagesGridContainer));