import React from 'react';
import Gallery from 'react-grid-gallery';

import './images-grid.style.css';

const overlayStyle = {
  maxHeight: '135px',
  overflow: 'hidden',
  position: 'absolute',
  width: '100%',
  backgroundColor: 'rgba(0, 0, 0, 0.8)',
  bottom: '0',
  borderRadius: '10px 10px 0 0',
  color: 'white'
};


const formatPhoto = ({ farm, id, ownername, secret, server, title, views, height_n, width_n, url_n, url_l }) => ({
  src: url_l,
  thumbnail: url_n,
  thumbnailWidth: parseInt(width_n, 10),
  thumbnailHeight: parseInt(height_n, 10),
  customOverlay: (
    <div style={overlayStyle}>
      <div className='overlay-text-container'>
        <p className='overlay-text'><span className='overlay-text-type'>Title</span><span className='overlay-text-detail'>{title}</span></p>
        <p className='overlay-text'><span className='overlay-text-type'>Owner</span><span className='overlay-text-detail'>{ownername}</span></p>
        <p className='overlay-text'><span className='overlay-text-type'>Views</span><span className='overlay-text-detail'>{views}</span></p>
      </div>
      
    </div>
  ),
  caption: `${title} - ${ownername} - ${views}`
});


class ImagesGridPresenter extends React.Component {
  render() {
    const { photos, rowHeight = 300, onClickThumbnail } = this.props;
    return (
      <div className='grid-container'>
        <Gallery
          rowHeight={rowHeight}
          images={photos.map(photo => formatPhoto(photo))}
          enableLightbox={true}
          enableImageSelection={false}
          onClickThumbnail={index => onClickThumbnail(photos[index].id)}
        />
      </div>
    );
  }

}

// const ImagesGridPresenter = ({ photos, rowHeight = 300, onClickThumbnail, onScroll }) => 
  

export default ImagesGridPresenter;