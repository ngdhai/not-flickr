import React from 'react';
import { Container, Row, Col } from 'reactstrap';

import './photo.style.css';

const PhotoPresenter = ({ photo, handleTagClick }) => 
  photo ? 
  <div className="photo-container">
    <div className="image-screen">
      <img alt="profile" src={`https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_b.jpg`}></img>
    </div>

    <Container className="info-container">
      <Row>
        <Col>
          <Row>
            <Col sm="2">
              <img className="profile-img" alt="profile" src={`http://farm${photo.owner.iconfarm}.staticflickr.com/${photo.owner.iconserver}/buddyicons/${photo.owner.nsid}.jpg`}></img>
            </Col>
            <Col sm="10">
              <div className="username">{photo.owner.username}</div>
              <div className="image-title">{photo.title._content}</div>

              <div className="description">
                {
                  photo.description._content.split('\n\n').map((item, i) => 
                    <p key={i}>{item}</p>
                  )
                }
              </div>
            </Col>
          </Row>
        </Col>
        <Col>
          <div className="tag-title">Tags</div>
          <div className="tags">
          {
            photo.tags.tag.map((item, i) => 
              <div key={i} className="tag-item" onClick={() => handleTagClick(item._content)}>{item.raw}</div>
            )
          }
          </div>
        </Col>
      </Row>
    </Container>
  </div>
  :
  null

export default PhotoPresenter;
