import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PhotoPresenter from './photo.presenter';
import { doFetchPhotoDetail, doFetchPhotos } from '../../actions/photo';

class PhotoContainer extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      photo: null
    };
  }

  handleTagClick = tag => {
    this.props.history.push(`/tag/${tag}`);
  }

  render() {
    const { photo } = this.props;
    console.log(photo);
    return(
      <PhotoPresenter 
        photo={photo}
        handleTagClick={this.handleTagClick}
      />
    );
  }

  componentDidMount() {
    const { onFetchPhotoDetail } = this.props;
    const { id } = this.props.match.params;
    onFetchPhotoDetail(id);
  }

}

const mapStateToProps = state => ({
  photo: state.photoState.singlePhoto
});

const mapDispatchToProps = dispatch => ({
  onFetchPhotoDetail: query => dispatch(doFetchPhotoDetail(query)),
  onFetchPhotos: query => dispatch(doFetchPhotos(query)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PhotoContainer)
);